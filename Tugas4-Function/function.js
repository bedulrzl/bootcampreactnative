//No. 1 
function teriak(){
    return  "Halon Sanbers!"
}

console.log(teriak());

console.log("========================================================")

//No. 2
var num1 = 12
var num2 = 4
function kalikan(angka1, angka2) {
    return angka1 * angka2
}

var hasikali = kalikan(num1, num2)
console.log(hasikali)
console.log("=========================================================")

//No.3
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

function introduce(nama, umur, alamat, hobi) {
    return `nama saya ${nama}, umur saya ${umur} tahun, alamat saya di ${alamat}, dan saya punya hobby yaitu ${hobi}`
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);