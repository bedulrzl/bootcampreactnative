// If-else

var nama = "junaedi"
var peran = "werewolf"

if (nama == "john" && peran == "") {
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else if (nama == "jane" && peran == "penyihir") {
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == "jenita" && peran == "guard") {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == "junaedi" && peran == "werewolf") {
    console.log("Selamat datang di Dunia Werewolf, junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Nama harus diisi!")
}


//switch

var tanggal = 31;
var bulan = 3;
var tahun = 1990;

switch (bulan) {
    case 1:
        console.log(tanggal + " januari " + tahun)
        break;
    case 2:
        console.log(tanggal + " februari " + tahun)
        break;
    case 3:
        console.log(tanggal + " maret " + tahun)
        break;
    case 4:
        console.log(tanggal + " april " + tahun)
        break;
    case 5:
        console.log(tanggal + " mei " + tahun)
        break;
    case 6:
        console.log(tanggal + " juni " + tahun)
        break;
    case 7:
        console.log(tanggal + " juli " + tahun)
        break;
    case 8:
        console.log(tanggal + " agustus " + tahun)
        break;
    case 9:
        console.log(tanggal + " september " + tahun)
        break;
    case 10:
        console.log(tanggal + " oktober " + tahun)
        break;
    case 11:
        console.log(tanggal + " nopember " + tahun)
        break;
    case 12:
        console.log(tanggal + " Desember " + tahun)
        break;
    default:
        console.log("harus di isi dahulu")
        break;
}