//No. 1 Looping While 

var angka = 2
var plus = 2

console.log("LOOPING PERTAMA")
while (angka <= 20) {
    console.log(angka + "- I love coding");
    angka += plus;

}
angka -= plus
console.log("LOOPING KEDUA")
while (angka > 0) {

    console.log(angka + "- I will become a mobile developer")

    angka -= plus;
}
console.log("==================================================================")

//No. 2 Looping menggunakan for
for (var i = 1; i <= 20; i++) {
    if (i % 2 != 0 && i % 3 == 0) {
        console.log(i + "- I Love Coding")
    } else if (i % 2 != 0) {
        console.log(i + "- santai ")
    } else if (i % 2 == 0) {
        console.log(i + "- berkualitas  ")
    }
}
console.log("==================================================================")


//No. 3 Membuat Persegi Panjang #

for (var i = 1; i <= 4; i++) {
    var draw = "";
    for (var x = 1; x <= 8; x++) {
        draw += "#";
    }
    console.log(draw);
}
console.log("==================================================================")

//No. 4 Membuat Tangga 

for (var i = 1; i <= 7; i++) {
    var draw = ""
    for (var x = 1; x <= i; x++) {
        draw += "#"
    }
    console.log(draw)
}
console.log("==================================================================")

//No. 5 Membuat Papan Catur
for (var i = 1; i <= 8; i++) {
    var draw = "";
    for (var x = 1; x <= 8; x++) {
        if ((i + x) % 2 == 0) {
            draw += " "

        } else {
            draw += "#"
        }
    }
    console.log(draw);
}