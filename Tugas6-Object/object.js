//1.Soal No. 1 (Array to Object)
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]

function arrayToObject(data) {
    var obj = []
    for (var i = 0; i < data.length; i++) {
        var now = new Date();
        var thisYear = now.getFullYear();
        if (!data[i][3] || data[i][3] > thisYear) {
            var umur = "invalid birth year"
        } else {
            var umur = thisYear - data[i][3]
        }
        var dataTemp = {
            firstname: data[i][0],
            lastname: data[i][1],
            gender: data[i][2],
            age: umur
        }

        var res = i + 1 + ". " + data[i][0] + ' ' + data[i][1];
        obj[res] = dataTemp

    }
    return obj;
}
console.log(arrayToObject(people));
console.log(arrayToObject(people2));
console.log("=================================================================")

//Soal No. 2 (Shopping Time)

function shoppingTime(memberId, money) {
    var result = []
    var listPurcashe = []
    var listmoney = []
    var changemoney = 0
    var barang = [
        ["sepatu stacattu", 1500000],
        ['baju zoro', 500000],
        ['baju H&N', 250000],
        ['sweater unikloh', 175000],
        ['casing handphone', 50000]
    ]

    for (var i = 0; i < barang.length; i++) {
        if (money > barang[i][1]) {
            listPurcashe.push(barang[i][0])
            listmoney.push(barang[i][1])
        }
    }
 
    if (!memberId) {
        console.log("mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        console.log("mohon maaf uang tidak cukup")
    } else {
        var sumMoney = money
        for (var x = 0; x < listmoney.length; x++) {
            changemoney = sumMoney -= listmoney[x]
        }
        var temp = {
            memberId: memberId,
            money: money,
            listPurcashed: listPurcashe,
            changeMoney: changemoney
        }
        result = temp
        console.log(result)
    }
}

shoppingTime('1820RzKrnWn08', 2475000)
shoppingTime('82Ku8Ma742', 170000)
shoppingTime('', 2475000)
shoppingTime('234JdhweRxa53', 15000)
shoppingTime()
console.log("===========================================================")

//Soal No. 3 (Naik Angkot)
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    const biaya = 2000;
    var hasil = [];
    
    for (let i = 0; i < listPenumpang.length; i++) {
      var penumpang = listPenumpang[i];
      var temp = {
          penumpang: penumpang[0],
          naikDari: penumpang[1],
          tujuan: penumpang[2],
          bayar: biaya * (rute.indexOf(penumpang[2]) - rute.indexOf(penumpang[1]))
      };
      hasil.push(temp);
    }
    
    return hasil;
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
