import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/app'
import LoginScreen from './Tugas/Tugas13/LoginScreen'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
import RegisterScreen from './Tugas/Tugas13/RegisterScreen'
import Todo from './Tugas/Tugas14/App'
import Tugas15 from './Tugas/Tugas15/index'
import Quiz3 from './Tugas/Quiz3/index'

export default function App() {
  return (
   // <YoutubeUI/>
   // <LoginScreen/>
   //* <RegisterScreen/> *//
  // <AboutScreen />
  // <Todo />
   //* <Tugas15 /> *//
   <Quiz3 />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
