import React, { Component } from "react";
import {
  Image,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
} from "react-native";

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.contaienr}>
        <Image
          source={require("./image/logo.png")}
          style={{ width: 375, height: 100 }}
        />
        <Text style={styles.loginText}>Login</Text>
        <View style={styles.body}>
            <View style={styles.formInput}>
                <Text>Username/Email</Text>
            <TextInput style={styles.input} />
            </View>
            <View style={styles.formInput}>
                <Text>Password</Text>
            <TextInput style={styles.input} />
            </View>
        </View>

        <View style={styles.boxLogin}>
            <TouchableOpacity style={styles.masuk}>
               <Text style={styles.textBtn}>Masuk</Text>
            </TouchableOpacity>
            <Text style={styles.textOr}>Atau</Text>
            <TouchableOpacity style={styles.daftar}>
               <Text style={styles.textBtn}>Daftar?</Text>
            </TouchableOpacity>
        </View>
        
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contaienr: {
    flex: 1,
    paddingTop: 63,
    alignItems:"center"
  },
  loginText: {
      fontSize: 24,
      color: '#003366',
      marginTop: 64
  },
  body:{
    marginVertical: 40
  },
  formInput: {
      marginVertical: 8
  },
  input: {
    height: 48,
    width:294,
    borderWidth:1,
    borderColor:'#003366',
    marginVertical: 5,
    paddingHorizontal: 8
  },
  boxLogin:{
    flex: 1,
    alignItems:"center"
  },
  masuk:{
      backgroundColor: '#3Ec6ff',
      paddingVertical:16,
      paddingHorizontal: 70,
      marginVertical: 16,
      borderRadius: 20
  }, 
  textBtn: {
      color: '#fff',
      fontSize:24
  },
  daftar:{
    backgroundColor: '#003366',
    paddingVertical:16,
    paddingHorizontal: 70,
    marginVertical: 16,
    borderRadius: 20
  },
  textOr: {
      color: '#3EC6FF',
      fontSize: 24
  }
});
