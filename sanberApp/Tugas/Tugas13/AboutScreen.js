import React, { Component } from 'react'
import {
    Image,
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    TextInput,
  } from "react-native";

import Icon from 'react-native-vector-icons/FontAwesome'

export default class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.profile}>
                    <Text style={styles.textAboutHeader}>Tentang Saya</Text>
                    <Icon style={styles.iconProfile} name='user-circle' size={130}/>
                    <Text style={styles.nameProfile}>Aprizal</Text>
                    <Text style={styles.jobProfile}>React Native Developer</Text>
                </View>
                <View style={styles.portofolio}>
                    <Text style={styles.textBox}>portofolio</Text>
                    <View style={styles.listPorto}>
                        <View style={styles.list}>
                            <Icon style={styles.iconPorto} name='gitlab' size={50}/>
                            <Text style={styles.textList}>@Aprizal</Text>
                        </View>
                        <View style={styles.list}>
                            <Icon style={styles.iconPorto}name='github' size={50}/>
                            <Text style={styles.textList}>@Aprizal</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.portofolio}>
                    <Text style={styles.textBox}>Hubungi Saya</Text>
                    <View style={styles.listContact}>
                        <View style={styles.listcon}>
                            <Icon style={styles.iconPorto} name='facebook-square' size={50}/>
                            <Text style={styles.textList}>@Aprizal</Text>
                        </View>
                        <View style={styles.listcon}>
                            <Icon style={styles.iconPorto}name='instagram' size={50}/>
                            <Text style={styles.textList}>@Aprizal</Text>
                        </View>
                        <View style={styles.listcon}>
                            <Icon style={styles.iconPorto}name='twitter' size={50}/>
                            <Text style={styles.textList}>@Aprizal</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    profile: {
        alignItems: 'center',
        marginTop: 64
    },
    textAboutHeader: {
        fontSize: 36,
        lineHeight:42,
        color:'#003366',
        fontWeight: '700'
    },
    iconProfile: {
        color:'#EFEFEF',
        marginVertical: 8
    },
    nameProfile: {
        fontSize: 24,
        color: '#003366',
        fontWeight: '700',
        lineHeight: 28
    },
    jobProfile: {
        fontSize:16,
        fontWeight:'700',
        lineHeight:19,
        color:'#3ec6ff',
        marginVertical: 8
    },
    portofolio:{
        margin:8,
        padding: 8,
        backgroundColor:'#efefef',
        borderRadius: 20
    },
    textBox: {
        fontSize: 18,
        color:'#003366',
        borderBottomWidth: 1,
        borderColor: '#003366',
        paddingBottom: 10
    },
    listPorto: {
        flexDirection:'row',
        justifyContent: 'space-around',
        alignItems:'center',
        padding: 20
    },
    list: {
        alignItems:'center'
    },
    iconPorto: {
        color:'#3ec6ff',
        marginVertical:8,
        marginHorizontal: 8,
        alignItems: 'center'
    },
    textList: {
        fontSize:16,
        color:'#003366',
        fontWeight:'700',
        marginHorizontal: 8

    },
    listContact:{
        alignItems: 'center',
        padding: 20
    },
    listcon: {
        flexDirection: 'row',
        alignItems: 'center',
    }

})