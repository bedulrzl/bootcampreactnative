//Soal No. 1 (Range) 
function range(starNum, finishNum) {
    var arr = [];

    if (starNum > finishNum) {
        for (x = finishNum; x <= starNum; x++) {
            arr.push(x)
            arr.sort(function (value1, value2) {
                return value2 - value1
            })
        }
    } else if (starNum < finishNum) {
        for (i = starNum; i <= finishNum; i++) {
            arr.push(i)
        }
    } else if (starNum == null || finishNum == null) {
        arr.push(-1)
    }

    return arr
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(3)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("====================================================================")

//Soal No. 2 (Range with Step)
function rangeWithStep(starNum, finishNum, step) {
    var arr2 = []

    if (starNum > finishNum) {
        for (var x = finishNum; x <= starNum; x += step) {

            arr2.push(x)
            arr2.sort(function (value1, value2) {
                return value2 - value1
            })
        }
    } else {
        for (x = starNum; x <= finishNum; x += step) {
            arr2.push(x)
        }
    }
    return arr2
}

console.log(rangeWithStep(1, 10, 1)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [26, 22, 18, 14, 10, 6,  2] 
console.log("====================================================================")

//Soal No. 3 (Sum of Range)
function sum(starNum, finishNum, step) {

    var sum1 = range(starNum, finishNum)
    var sum2 = rangeWithStep(starNum, finishNum, step)

    var hasil = 0

    if (starNum == null || finishNum == null) {
        hasil += starNum
    } else if (starNum == null && finishNum == null && step == null) {
        hasil += 2
    } else if (step = step) {
        for (var i = 0; i < sum2.length; i++) {
            hasil += sum2[i]
        }
    } else if (starNum === starNum && finishNum === finishNum) {
        for (var i = 0; i < sum1.length; i++) {
            hasil += sum1[i]
        }
    }

    return hasil

}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("====================================================================")

//Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]


function dataHandling(data) {
    for (var i = 0; i < data.length; i++) {
        var tampung = data[i];
        for (var j = 0; j < data.length + 1; j++) {
            console.log(`Nomor ID: ${tampung[j,0]}`)
            console.log(`Nama: ${tampung[j,1]}`)
            console.log(`TTL: ${tampung[j,2] } ${tampung[j,3]}`)
            console.log(`Hobi: ${tampung[j,4]}`)
            console.log("                          ")
        }

    }
    return tampung;

}
dataHandling(input)

//Soal No. 5 (Balik Kata)
function balikKata(str) {
    var newString = ""
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }

    return newString;
}
console.log("====================================================")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("====================================================")


//soal No. 6
var input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(data) {
    data.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(data);

    var bulan = data[3].split("/")
    var hasilbulan = bulan.slice(1, 2)
 
    if (hasilbulan == '05') {
        console.log("Mei")
    }
    var reverser =  bulan.reverse()
    console.log(reverser)
    var date = data[3].split("/")
    var joine = date.join("-")
    console.log(joine)

    var nama = data[1].slice(0,15)
    console.log(nama)

}

dataHandling2(input2)